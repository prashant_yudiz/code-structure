﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	public class UIView : MonoBehaviour 
	{
		public GameObject content;

		public virtual void Enable()
		{
			content.SetActive(true);
		}

		public virtual void Disable()
		{
			content.SetActive(false);

		}
	}
