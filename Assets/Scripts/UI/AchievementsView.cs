﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsView : UIView {

	public override void Enable()
	{
		base.Enable();
	}

	public override void Disable()
	{
		base.Disable();
	}

	public void OnBack()
	{
		GameStateManager.instance.UpdateState(GameStates.MainMenu);
	}
}
