﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionsView :  UIView 
{

	public ScrollRect scrollrect;
	bool canScroll = true;
	public override void Enable()
	{
		base.Enable();
		Init();
	}

	void Init()
	{
		scrollrect.horizontalNormalizedPosition = 0f/9f;
		canScroll = true;
	}

	public override void Disable()
	{
		base.Disable();
	}

	public void OnBack()
	{
		GameStateManager.instance.UpdateState(GameStates.MainMenu);
	}

	public void OnNext()
	{
		if(!canScroll)
		return;

		if((scrollrect.horizontalNormalizedPosition)<0.95f)
		{
//			scrollrect.horizontalNormalizedPosition+=  (1f/8f);
			StartCoroutine(SmoothScrollTo(scrollrect.horizontalNormalizedPosition, scrollrect.horizontalNormalizedPosition+(1f/8f), 0.2f));

		}

//		Debug.Log((scrollrect.horizontalNormalizedPosition));

	}

	public void OnPrevious()
	{
		if(!canScroll)
		return;

		if((scrollrect.horizontalNormalizedPosition)>0.05f)
		{
			
//			scrollrect.horizontalNormalizedPosition-= (1f/8f );
			StartCoroutine(SmoothScrollTo(scrollrect.horizontalNormalizedPosition, scrollrect.horizontalNormalizedPosition-(1f/8f), 0.2f));
		}
//		Debug.Log((scrollrect.horizontalNormalizedPosition));

	}

	IEnumerator SmoothScrollTo(float fromVal, float toVal, float duration)
	{
		canScroll = false;
		float elapsed=0;
		while (elapsed<duration)
		{
//			Debug.Log(fromVal+" "+toVal);
			scrollrect.horizontalNormalizedPosition = Mathf.Lerp(fromVal, toVal, elapsed/duration);
			elapsed+=Time.deltaTime;
			yield return null;
		}
		scrollrect.horizontalNormalizedPosition = toVal;

		canScroll = true;
		yield return null;
	}


}
	