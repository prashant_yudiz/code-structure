﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsView : UIView 
{
	public GameObject musicToggle;
	public GameObject soundToggle;

	public override void Enable()
	{
		base.Enable();
		Init();
	}

	void Init()
	{
		if(Prefs.Music==0)
		{
			musicToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, 0);
		}
		else
		{
			musicToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 0);

		}

		if(Prefs.Sound==0)
		{

			soundToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, 0);
		}
		else
		{

			soundToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 0);

		}
	}

	public override void Disable()
	{
		base.Disable();
	}

	public void OnBack()
	{
		GameStateManager.instance.UpdateState(GameStates.MainMenu);
	}

	public void OnMusic()
	{
		if(Prefs.Music==0)
		{
			
			Prefs.Music=1;
			musicToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 0);
		}
		else
		{
			Prefs.Music=0;

			musicToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, 0);

		}
	}

	public void OnSound()
	{
		if(Prefs.Sound==0)
		{
			Prefs.Sound=1;
			soundToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(50, 0);
		}
		else
		{
			Prefs.Sound=0;
			soundToggle.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, 0);

		}
	}
}
