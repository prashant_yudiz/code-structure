﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuView : UIView 
{

	public override void Enable()
	{
		base.Enable();
	}

	public override void Disable()
	{
		base.Disable();
	}



	public void OnSinglePlayer()
	{
		GameStateManager.instance.UpdateState(GameStates.SinglePlayer);
	}

	public void OnTwoPlayers()
	{
		GameStateManager.instance.UpdateState(GameStates.TwoPlayers);
	}

	public void OnMissions()
	{
		GameStateManager.instance.UpdateState(GameStates.Missions);
	}

	public void OnAchievements()
	{
		GameStateManager.instance.UpdateState(GameStates.Achievements);
	}
			
	public void OnSettings()
	{
		GameStateManager.instance.UpdateState(GameStates.Settings);
	}

	public void OnHelp()
	{
		GameStateManager.instance.UpdateState(GameStates.Help);
	}

	public void Back()
	{
		GameStateManager.instance.UpdateState(GameStates.MainMenu);
	}
}
