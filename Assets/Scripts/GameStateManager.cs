﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour 
{
	public static GameStateManager instance;
	public GameObject[] UIViews;
	public GameStates currentState;
	public GameStates previousState;

	public delegate void OnStageChangeEvent(GameStates state);
	public static event OnStageChangeEvent OnStateChange;

	
	void Awake ()
	{
		DontDestroyOnLoad(gameObject);

		if (instance == null)
			instance = this;

		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}			
	}

	void Start()
	{	
		UpdateState(GameStates.MainMenu);
	}

	public void UpdateState(GameStates toState)
	{
		UIViews [(int)currentState].GetComponent<UIView>().Disable();		
		UIViews [(int)toState].GetComponent<UIView>().Enable ();		
		previousState = currentState;
		currentState = toState;
		OnStateChange(currentState);
	}

}



