﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotManager : MonoBehaviour 
{
	public static BotManager instance	;
	public bool debugPath;
	void Awake()
	{
		instance = this;
	}

	public void ExecuteBotsTurn(GameObject[] myBases, GameObject[] enemyBases)
	{
		//select a random base
		GameObject selectedBase = myBases[Random.Range(0, myBases.Length)];
		GameObject selectedEnemyBase = enemyBases[Random.Range(0, enemyBases.Length)];

		Vector3 source = selectedBase.transform.position;
		//select a random enemy base's x position
		//add offset to enemy base's x position (1 in 3 should hit)
		float error = Random.Range(-1f, 1f);
		Vector3 dest = new Vector2(selectedEnemyBase.transform.position.x + error, selectedEnemyBase.transform.position.y);
		
		//get a single or double deviation path 
		float deviation = Random.Range(-2f, 2f);

		Vector3[] deviationPoints = new Vector3[2];
		float[] deviationYPos = new float[]{Random.Range(0.8f, 1.4f), Random.Range(-0.8f, -1.4f)};

		deviationPoints[0] = new Vector3(selectedBase.transform.position.x+deviation, deviationYPos[0], 0);
		deviationPoints[1] = new Vector3(selectedEnemyBase.transform.position.x+(+deviation), deviationYPos[1], 0);

		//create path
		Vector3[] junctions = new Vector3[]{source, deviationPoints[0], deviationPoints[1], dest};


		//shoot rocket on the path

	}

}
