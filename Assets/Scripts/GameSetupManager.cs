﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetupManager : MonoBehaviour 
{
	public static GameSetupManager instance;
	Camera mainCam;

	public GameObject basePrefab;
	public List<GameObject> player1Bases = new List<GameObject>();
	public List<GameObject> player2Bases = new List<GameObject>();

	public GameSetupStates currentState;
	public GameObject[] baseHolders = new GameObject[2];
	public int maxBasesAllowed;
	void Awake()
	{
		instance = this;
		mainCam = Camera.main;

	}

	public void Init()
	{
		currentState = GameSetupStates.PlacingBase1;
	}

	void PlaceBase(Vector2 clickPos)
	{
		if(currentState == GameSetupStates.PlacingBase1)
		{

			GameObject tempBase = (GameObject)Instantiate(basePrefab, clickPos, Quaternion.identity);
			tempBase.transform.SetParent(baseHolders[0].transform);
			player1Bases.Add(tempBase);
			if(player1Bases.Count>=3)
			{
				currentState = GameSetupStates.PlacingBase2;
	//			GameplayManager.instance.Init();
			}
		}

		else if (currentState == GameSetupStates.PlacingBase2)
		{
			GameObject tempBase = (GameObject)Instantiate(basePrefab, clickPos, Quaternion.identity);
			tempBase.transform.SetParent(baseHolders[1].transform);
			player2Bases.Add(tempBase);
			if(player2Bases.Count>=3)
			{
				currentState = GameSetupStates.None;
				GameplayManager.instance.Init();
			}
		}

	}

	Vector2 mousePosition;

	void Update()
	{
		if(GameStateManager.instance.currentState == GameStates.TwoPlayers || GameStateManager.instance.currentState == GameStates.SinglePlayer)
		{
			if(currentState == GameSetupStates.PlacingBase1)
			{
				if(Input.GetMouseButtonDown(0))
				{
					mousePosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
					if(Physics2D.OverlapCircle(mousePosition,0.2f)!=null)
					{
						Collider2D col = Physics2D.OverlapCircle(mousePosition,0.2f);
						if(col.gameObject.name == "Bottom")
						PlaceBase(mousePosition);
					}
				}
			}
			else if(currentState == GameSetupStates.PlacingBase2)
			{
				if(GameStateManager.instance.currentState == GameStates.TwoPlayers)
				{
					if(Input.GetMouseButtonDown(0))
					{
						mousePosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
						if(Physics2D.OverlapCircle(mousePosition,0.2f)!=null)
						{
							Collider2D col = Physics2D.OverlapCircle(mousePosition,0.2f);
							if(col.gameObject.name == "Top")
							PlaceBase(mousePosition);
						}
					}
				}
				else
				{
					PlaceBotBases();
				}
			}
		}
	}

	public Transform[] botBasesPos;
	void PlaceBotBases()
	{
		for(int i=0; i<botBasesPos.Length; i++)
		{
			GameObject tempBase = (GameObject)Instantiate(basePrefab, botBasesPos[i].position, Quaternion.identity);
			tempBase.transform.SetParent(baseHolders[1].transform);
			player2Bases.Add(tempBase);
		}
		currentState = GameSetupStates.None;
		GameplayManager.instance.Init();
	}
}
