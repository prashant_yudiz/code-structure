﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Prefs
{

	public static int Music
	{
		get { return PlayerPrefs.GetInt( "music", 1 ); }
		set { PlayerPrefs.SetInt( "music", value ); }
	}

	public static int Sound
	{
		get { return PlayerPrefs.GetInt( "sound", 1 ); }
		set { PlayerPrefs.SetInt( "sound", value ); }
	}

}
