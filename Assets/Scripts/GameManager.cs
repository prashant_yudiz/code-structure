﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance;
	public GameObject World;

	void Awake()
	{
		instance = this;
	}

	void OnEnable()
	{
		GameStateManager.OnStateChange+=OnStateChange;
	}

	void OnDisable()
	{
		GameStateManager.OnStateChange-=OnStateChange;
	}


	void OnStateChange(GameStates state)
	{
		//use enums

		switch (state)
		{

		case GameStates.MainMenu:
			World.SetActive(false);
		break;

		case GameStates.SinglePlayer:
			InputProcessor.instance.Reset();
			GameSetupManager.instance.Init();
			World.SetActive(true);
		break;

		case GameStates.TwoPlayers:
			GameSetupManager.instance.Init();
			World.SetActive(true);
			break;

		case GameStates.Missions:
			World.SetActive(false);
		break;

		case GameStates.Achievements:
			World.SetActive(false);
		break;

		case GameStates.Settings:
			World.SetActive(false);
		break;

		case GameStates.Help:
			World.SetActive(false);
		break;

		default:
			break;
		}	
	}
}
