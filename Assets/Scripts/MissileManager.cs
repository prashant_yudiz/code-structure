﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileManager : MonoBehaviour 
{
	public static MissileManager instance;

	public GameObject missilePrefab;

	GameObject currentMissile;
	float percentsPerSecond = 0.5f;
    float currentPathPercent;

    
	void Awake()
	{
		instance = this;

	}

	Vector2 lastPos, currentPos;


	Vector3 dir;

	public void DestroyThisMissile(GameObject _missile)
	{
		currentMissile  = _missile;
		Destroy(_missile);
		InputProcessor.instance.Reset();
		GameplayManager.instance.ChangeTurn();

	}

	public void DestroyCurrentMissile()
	{
		Destroy(currentMissile);
		GameplayManager.instance.ChangeTurn();
	}

	public IEnumerator LaunchMissile (Vector3[] path)
	{
		GameplayManager.instance.currentState = GameplayStates.WaitForTurn;
		GameObject missile = Instantiate(missilePrefab, currentPos, Quaternion.identity);
		missile.GetComponent<Missile>().Launch();
		currentPathPercent=0f;
		while (currentPathPercent<1 && missile!=null)
		{
			currentPathPercent += percentsPerSecond * Time.deltaTime;
		
			lastPos = currentPos;
				
			currentPos = missile.transform.position;


			dir = currentPos - lastPos;
			
			var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			missile.transform.rotation = Quaternion.AngleAxis(angle+270, Vector3.forward);
			yield return null;
		}

		StartCoroutine(NudgeMissile(missile));
		Debug.Log("End of path");

		yield return null;
	}

	IEnumerator NudgeMissile (GameObject _missile)
	{
		while (_missile!=null)
		{
			_missile.transform.position+=_missile.transform.up *2f * Time.deltaTime;

			yield return null;
		}
		yield return null;
	}
}