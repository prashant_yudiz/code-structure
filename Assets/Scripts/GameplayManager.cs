﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour 
{
	public static GameplayManager instance;
	public GameplayStates currentState;
	public int lastTurn = 1;

	void Awake()
	{
		instance = this;
	}

	public void Init()
	{
		currentState = GameplayStates.Player1Turn;
	}

	public void ChangeTurn()
	{
		Debug.Log("Chaning turn.");
		if(lastTurn==1)
		{

			lastTurn = 2;

			if(GameStateManager.instance.currentState == GameStates.SinglePlayer)
			{
				BotsTurn();
				currentState = GameplayStates.BotsTurn;

			}
			else
			{
				currentState = GameplayStates.Player2Turn;

			}
			Debug.Log("Player 2's turn.");

		}
		else
		{
			lastTurn = 1;
			currentState = GameplayStates.Player1Turn;
			Debug.Log("Player 1's turn.");

		}
	}

	public void BotsTurn()
	{
		Debug.Log("Bot's turn.");

		BotManager.instance.ExecuteBotsTurn(GameSetupManager.instance.player2Bases.ToArray(), GameSetupManager.instance.player1Bases.ToArray());
	}
}

