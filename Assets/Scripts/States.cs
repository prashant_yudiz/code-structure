﻿public enum GameplayStates
{
	None,
	Player1Turn,
	Player2Turn,
	BotsTurn,
	WaitForTurn,
}

public enum GameStates
{
	MainMenu = 0,
	SinglePlayer = 1,
	TwoPlayers = 2,
	Missions =3,
	Achievements = 4,
	Settings = 5,
	Help =6
}

public enum GameSetupStates
{
	None,
	PlacingBase1,
	DrawingTerrain1,
	PlacingBase2,
	DrawingTerrain2

}